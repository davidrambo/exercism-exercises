-- Find the difference between the square of the sum and the sum of the squaers of
-- the first N natural numbers:
-- (1 + 2 + .. + N)^2 - (1^2 + 2^2 + .. + N^2)
--
-- Source: Project Euler #6

-- Gauss figured out that the sum of natural numbers 1 to n is
-- n(n+1) / 2
local function square_of_sum(n)
  return ((n * (n + 1)) / 2)^2
end

-- ...and that the sum of squares is
-- n(n+1)(2n+1) / 6
local function sum_of_squares(n)
  return (n * (n + 1) * ((2 * n) + 1)) / 6
end

local function difference_of_squares(n)
  return square_of_sum(n) - sum_of_squares(n)
end

return {
  square_of_sum = square_of_sum,
  sum_of_squares = sum_of_squares,
  difference_of_squares = difference_of_squares,
}
