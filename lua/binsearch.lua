local bins =  function(array, target)
  local a = 1 -- start index
  local b = #array -- end index

  -- local b = 0 -- end index
  -- for _ in pairs(array) do
  --   b = b + 1
  -- end

  local mid = math.floor((a + b) / 2)

  while a <= b do
    if array[mid] == target then
      return mid
    elseif array[mid] < target then
      a = mid + 1
    else
      b = mid - 1
    end
    mid = math.floor((a + b) / 2)
  end

  return -1
end

print("Testing empty array...")
print(bins({}, 6))
print("Next answer should be 8...")
print(bins({ 6, 67, 123, 345, 456, 457, 490, 2002, 54321, 54322 }, 2002))
