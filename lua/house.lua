--[[ Recite the nursery rhyme with a recursive process that embeds previous arguments in future calls.
-- For example:
-- 
-- 1: This is the house that jack built.
--
-- 2: This is the malt
--    that lay in the house that jack built.
--
-- 3: This is the rat
--    that ate the malt
--    that lay in the house that jack built.
--
--  Etc.
--
--  Basic form:
--  This is the <arg1>
--  that <arg2> the <previous arg1>
--  that <previous arg2> the <...>
--  that lay in the house that jack built.
--    ]]

local house = {}

house.parts = {
  { "house that Jack built.", "lay in" },
  { "malt", "ate" },
  { "rat", "killed" },
  { "cat", "worried" },
  { "dog", "tossed" },
  { "cow with the crumpled horn", "milked" },
  { "maiden all forlorn", "kissed" },
  { "man all tattered and torn", "married" },
  { "priest all shaven and shorn", "woke" },
  { "rooster that crowed in the morn", "kept" },
  { "farmer sowing his corn", "belonged to" },
  { "horse and the hound and the horn", nil },
}

house.verse = function(which)
  local composition = "This is the " .. house.parts[which][1]
  if which == 1 then
    return composition
  end
  for i = which, 2, -1 do
    composition = composition .. "\nthat " .. house.parts[i - 1][2] .. " the " .. house.parts[i - 1][1]
  end
  return composition
end

house.recite = function()
  local recital = ""
  for i = 1, 12 do
    recital = recital .. house.verse(i) .. "\n"
  end
end

return house
