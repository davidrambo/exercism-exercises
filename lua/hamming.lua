-- Calculate the Hamming Distance of two strings if of equal length,
-- else throw an exception.

local a = "arst"
local b = "asdf"

-- for c1 in a:gmatch(".") do
--   print(c1)
-- end
local hamming = {}

-- hamming.compute = function(str1, str2)
-- is the same as:
function hamming.compute(str1, str2)
  if #str1 ~= #str2 then
    return -1
  end
  local t = 0
  for i = 1, #str1 do
    local c1 = str1:sub(i, i)
    local c2 = str2:sub(i, i)
    -- if c1 ~= c2 then
    --   t = t + 1
    -- end
    t = t + (c1 == c2 and 0 or 1)
    -- The parenthetical expression is like C's ternary expression.
    -- It only moves beyond the `and` operator if c1 ~= c2.
    -- Between 0 and 1, 0 is false, so it goes to 1.
  end
  return t
end

print(hamming.compute(a, b))
-- is the same as:
-- print(hamming["compute"](a, b))
