(import (rnrs))

; TODO: convert to lower or upper

(define scores-map
  '(("aeioulnrst" 1)
    ("dg" 2)
    ("bcmp" 3)
    ("fhvwy" 4)
    ("k" 5)
    ("jx" 8)
    ("qz" 10)))

(define (convert-score letter) ;; Takes a character and returns its score value.
  (define (string-contains? ch string-of-tiles)
    (define (iter str-list) ;; Goes through the characters in a scores-map string.
      (cond ((null? str-list) #f)
            ((eq? ch (car str-list)) #t)
            (else (iter (cdr str-list)))))
    (iter (string->list string-of-tiles)))
  (define (find-tile tiles-to-score)
    (let ((score-tiles (caar tiles-to-score))
          (score-value (cadar tiles-to-score)))
      (cond ((null? tiles-to-score) 0)
           ((string-contains? letter score-tiles) score-value)
           (else (find-tile (cdr tiles-to-score))))))
  (find-tile scores-map))

#|
The idea here is effectively a reduce that converts each element (a letter) to
its corresponding score value in scores-map and then returns the sum.
|#
(define (score word)
  (define (letters-to-total letters total)
    (if (null? letters)
      total
      (letters-to-total (cdr letters)
                        (+ total (convert-score (car letters))))))
  (letters-to-total (string->list (string-downcase word)) 0))
