(import (rnrs))
(import (ice-9 regex))
#| Acronym: convert strings to acronyms.
For example:
- "Portable Network Graphs" -> "PNG"
- "Liquid-crystal Display" -> "LCD"
- "First _in_ First Out" -> "FIFO"

(char-upcase chr) -> returns the uppercase character version of chr

(import (ice-9 regex))
(string-match REGEX str) -> returns the starting index and length of matched
                            substring
Pass the above to (match:substring <here>) to get the matched substring.
Better for this question is to use it with map and list-matches:
(map match:substring (list-matches ptr str))

1. Create a list of words:
  (map match:substring (list-matches "[a-zA-Z']+" test))
2. Take the length-1 0th substring in each string and upcase it.
  (map (lambda (word) (string-upcase (substring word 0 1))) list-of-words)
3. Append each from left to right:
  (foldr append "" lst-of-upcases)
|#
(define (foldl proc init seq)
  (define (iter acc items)
    (if (null? items)
      acc
      (iter (proc acc (car items)) (cdr items))))
  (iter init seq))

(define (acronym test)
  (foldl string-append "" (map (lambda (word) (string-upcase (substring word 0 1)))
                               (map match:substring (list-matches "[a-zA-Z']+" test)))))
  

#|(define (split-chr? chr)
  (if (or (equal? chr #\ )
          (equal? chr #\_)
          (equal? chr #\-))
    #t
    #f))

(define (acronym test)
  (define (iter acc words)
    (cond ((null? (cdr words)) (list->string (reverse acc)))
          ((split-chr? (car words))
           (iter (cons (char-upcase (cadr words)) acc)
                 (cdr words)))
          (else (iter acc (cdr words)))))
  (iter (string->list (substring test 0 1)) (string->list test))) |#
