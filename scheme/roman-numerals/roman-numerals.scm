(import (rnrs))

#|
- iterate through the roman-map pairs, let's call each pair a numeral-pair.
  - if n is greater than the arabic (cadr numeral-pair),
    then move on to next pair
  - if not, then subtract arabic from n, and set n to that,
    adding the numeral to a string
|#
(define roman-map
  '(("M" 1000)
    ("CM" 900)
    ("D" 500)
    ("CD" 400)
    ("C" 100)
    ("XC" 90)
    ("L" 50)
    ("XL" 40)
    ("X" 10)
    ("IX" 9)
    ("V" 5)
    ("IV" 4)
    ("I" 1)))

(define (roman n)
  (define (convert n numerals-arabics numeral-string)
    (let ((numeral-pair (car numerals-arabics))) ;; e.g. ("M" 1000)
      (cond ((= n 0) numeral-string)
            ((< n (cadr numeral-pair)) ;; e.g. n < 1000
             (convert n (cdr numerals-arabics) numeral-string))
            (else (convert (- n (cadr numeral-pair)) ;; e.g. n >= 1000
                           numerals-arabics
                           (string-append numeral-string (car numeral-pair)))))))
  (convert n roman-map ""))
  
