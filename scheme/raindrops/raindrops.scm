(import (rnrs))

#| (define (pling n)
  (if (= (modulo n 3) 0)
    (cons "Pling" (plang n))
    (plang n)))

(define (plang n)
  (if (= (modulo n 5) 0)
    (cons "Plang" (plong n))
    (plong n)))

(define (plong n)
  (if (= (modulo n 7) 0)
    (cons "Plong" '())
    '()))

(define (convert number)
  (let ((raindrops (pling number)))
       (if (null? raindrops)
         (number->string number)
         (string-join raindrops "")))) |#
  
(define (convert number)
  (let ((raindrops (string-append
                     (if (= (modulo number 3) 0) "Pling" "")
                     (if (= (modulo number 5) 0) "Plang" "")
                     (if (= (modulo number 7) 0) "Plong" ""))))
    (if (equal? "" raindrops)
      (number->string number)
      raindrops)))
