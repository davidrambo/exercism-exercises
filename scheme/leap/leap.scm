(import (rnrs))

(define (leap-year? year)
  (cond ((= (modulo year 400) 0) #t)
        ((= (modulo year 100) 0) #f)
        ((= (modulo year 4) 0) #t)
        (else #f)))
#| First iteration: thinking through in nested if statements
(define (leap-year? year)
  (define (mod4 n)
    (if (= (modulo n 4) 0)
      #t
      #f))
  (define (mod100 n)
    (if (= (modulo n 100) 0)
      #t
      #f))
  (define (mod400 n)
    (if (= (modulo n 400) 0)
      #t
      #f))
  (if (not (mod4 year))
    #f
    (if (not (mod100 year))
      #t
      (if (mod400 year)
        #t
        #f))))

|#
