/**
 * Returns a string dependent upon the number provided.
 *
 * @param {any} num the number to be converted
 * @returns {sound} string of raindrop sounds or number
 */
export const convert = (num) => {
  let sound = "";

  if (num % 3 === 0) {
    sound += "Pling";
  }

  if (num % 5 === 0) {
    sound += "Plang";
  }

  if (num % 7 === 0) {
    sound += "Plong";
  }

  return sound !== "" ? sound : num.toString();
};
