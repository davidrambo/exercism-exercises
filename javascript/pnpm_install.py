"""Script to run `pnpm install` in all JS exercism.io exercises that need it.

It creates a list of directories in ~/exercism/javascript/, iterates through them,
and if pnpm-lock.yaml does not exist, then it runs.
"""

import os
from pathlib import Path


def find_dirs() -> list[Path]:
    """Returns a list of the directories in which a pnpm-lock.yaml does not exist."""
    base_path = Path(Path.home(), "exercism", "javascript")

    install_directories: list[str] = []

    for exercise_dir in base_path.iterdir():
        if exercise_dir.is_dir():
            if not Path(exercise_dir, "pnpm-lock.yaml").is_file():
                install_directories.append(exercise_dir)

    return install_directories


def run_pnpm_install(dirs: list[Path]):
    """Runs `pnpm install` in the directories listed."""
    for path in dirs:
        os.chdir(path)
        os.system("pnpm install")


if __name__ == "__main__":
    run_pnpm_install(find_dirs())
