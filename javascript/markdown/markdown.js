/** Refactor to be more readable. Initially, this code passes a lot of arrays of length
 * two. The first element is a string, and the second is a boolean indicating whether a
 * list is being parsed. The `isTag` function is unused, so perhaps that is a clue as to
 * where to begin.
 */

/**
 * Returns the given text enclosed by the given html tag, adding brackets.
 */
function wrap(text, tag) {
  return `<${tag}>${text}</${tag}>`;
}

function isTag(text, tag) {
  return text.startsWith(`<${tag}>`);
}

function parser(markdown, delimiter, tag) {
  const pattern = new RegExp(`${delimiter}(.+)${delimiter}`);
  const replacement = `<${tag}>$1</${tag}>`;
  return markdown.replace(pattern, replacement);
}

// Rename from parse__
function parseStrong(markdown) {
  return parser(markdown, "__", "strong");
}

// Rename from parse_
function parseEmph(markdown) {
  return parser(markdown, "_", "em");
}

function parseText(markdown, list) {
  const parsedText = parseEmph(parseStrong(markdown));
  if (list) {
    return parsedText;
  } else {
    return wrap(parsedText, "p");
  }
}

/**
 * @param {any} markdown string in markdown syntax
 * @param {any} list ?
 */
function parseHeader(markdown, list) {
  // Count the number of hashes for the header level.
  let count = 0;
  for (let i = 0; i < markdown.length; i++) {
    if (markdown[i] === "#") {
      count += 1;
    } else {
      break;
    }
  }
  if (count === 0 || count > 6) {
    // Not a header, so return to parseLines and move onto next possible kind.
    return [null, list];
  }
  const headerTag = `h${count}`;
  const headerHtml = wrap(markdown.substring(count + 1), headerTag);
  // `markdown.substring(count + 1)` removes the leading hashes and space

  // A header indicates an end of a list if there was one preceding it.
  if (list) {
    return [`</ul>${headerHtml}`, false];
  } else {
    return [headerHtml, false];
  }
}

function parseLineItem(markdown, list) {
  if (markdown.startsWith("*")) {
    // List item found.
    const innerHtml = wrap(parseText(markdown.substring(2), true), "li");
    if (list) {
      // Already in a list.
      return [innerHtml, true];
    } else {
      // New list starting, so provide outer unordered list tag.
      return [`<ul>${innerHtml}`, true];
    }
  }

  // Not a list, so return to parseLine and move to next kind of line.
  return [null, list];
}

/**
 * @param {string} markdown string of one line in markdown syntax to be parsed
 * @param {boolean} list whether the previous line was a list item
 */
function parseParagraph(markdown, list) {
  if (!list) {
    return [parseText(markdown, false), false];
  } else {
    return [`</ul>${parseText(markdown, false)}`, false];
  }
}

/**
 * @param {string} markdown string in markdown syntax
 * @param {bool} list true if in list
 * @returns {[string, bool]} parsed line and a boolean to indicate whether in a list
 */
function parseLine(markdown, list) {
  const parsers = [parseHeader, parseLineItem, parseParagraph];

  for (const prsr of parsers) {
    const [result, inList] = prsr(markdown, list);
    if (result !== null) {
      return [result, inList];
    }
  }
}

/**
 * Splits string into array of separate lines. Iterates over each line, passing to
 * `parseLine` and appending the parsed string (now in html syntax) to `result`.
 * The second argument, `list`, is a boolean that tracks whether the current line
 * to be parsed is embedded within a list.
 *
 * @param {string} markdown string in markdown syntax
 */
export function parse(markdown) {
  const lines = markdown.split("\n");
  let result = "";
  let list = false;

  for (let i = 0; i < lines.length; i++) {
    let [lineResult, newList] = parseLine(lines[i], list);
    result += lineResult;
    list = newList;
  }

  if (list) {
    return result + "</ul>";
  } else {
    return result;
  }
}
