/** Looks for marks that would interrupt a horizontal side.
 */
const isHorizontalCont = (line, start, end) => {
  for (let idx = start; idx < end + 1; idx++) {
    if (line[idx] === " " || line[idx] === "|") return false;
  }
  return true;
};

/** Looks for marks that would interrupt a vertical side.
 */
const isVerticalCont = (strings, col, start, end) => {
  for (let idx = start; idx < end + 1; idx++) {
    if (strings[idx][col] === " " || strings[idx][col] === "-") return false;
  }
  return true;
};

export function count(strings) {
  const search = (x, y) => {
    // Arrays of indices at which corners are located.
    let x_crosses = [];
    let y_crosses = [];

    let line = strings[y];

    // Find contiguous corners in current row from x.
    for (let x_idx = x + 1; x_idx < strings[y].length; x_idx++) {
      if (line[x_idx] === "+") x_crosses.push(x_idx);
      else if (line[x_idx] === " ") break;
    }

    // Find contiguous corners in current column from y.
    for (let y_idx = y + 1; y_idx < strings.length; y_idx++) {
      line = strings[y_idx];
      if (line[x] === "+") y_crosses.push(y_idx);
      else if (line[x] === " ") break;
    }

    let local_total = 0;

    for (const x_pos of x_crosses) {
      for (const y_pos of y_crosses) {
        if (strings[y_pos][x_pos] === "+") {
          // Check for contiguity back to the corners.
          if (
            isHorizontalCont(strings[y_pos], x, x_pos) &&
            isVerticalCont(strings, x_pos, y, y_pos)
          )
            local_total++;
        }
      }
    }
    return local_total;
  };

  let total = 0;

  for (let y = 0; y < strings.length; y++) {
    for (let x = 0; x < strings[y].length; x++) {
      if (strings[y][x] === "+") total += search(x, y);
    }
  }

  return total;
}
