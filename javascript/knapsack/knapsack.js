/** Determines the greatest value of items to take within the weight limit.
 *
 * Uses a dynamic programming approach, which fills in a matrix with items.length-
 * number of arrays, each with maximumWeight+1-number of cells. m[0] is initialized
 * to all 0s. Then, for each item, each weight value is assessed. If the current item
 * weighs too much, then simply fill in the previous item array's (m[i-1]) value at
 * that weight. If the current item does not weight too much, then add the max of
 * that previous item-by-weight value vs. the current value plus the value at the cell
 * in the previous item array at a weight equal to the current considered weight minus
 * the current item's weight (i.e. greatest value for the weight that remains after
 * taking the current item).
 *
 * @param {number} maximumWeight value that the weight of selected items cannot exceed
 * @param {Array} items array of objects with `weight` and `value` keys.
 */
export const knapsack = (maximumWeight, items) => {
  const count = items.length;
  if (count === 0) return 0;

  // Create matrix for dynamic programming solution.
  const m = [];
  m[0] = Array(maximumWeight + 1).fill(0);

  let idx = 1; // For subscripting m.
  items.forEach((item) => {
    let value = item.value;
    let weight = item.weight;

    m[idx] = Array(maximumWeight + 1).fill(0);

    for (let w = 0; w < maximumWeight + 1; w++) {
      if (weight > w) {
        m[idx][w] = m[idx - 1][w];
      } else {
        m[idx][w] = Math.max(m[idx - 1][w], m[idx - 1][w - weight] + value);
      }
    }

    idx++;
  });

  return m[count][maximumWeight];
};
