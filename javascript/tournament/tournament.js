const createNewTeam = (tally, teamName) => {
  tally[teamName] = { MP: 0, W: 0, D: 0, L: 0, P: 0 };
  return tally;
};

export const tournamentTally = (results) => {
  let output = "Team                           | MP |  W |  D |  L |  P";
  if (!results) return output;

  let tally = {};

  let teamNames = []; // This will be used to sort the teams by number of points.

  // Iterate over each game outcome, one per line in the parameter string.
  const splitResults = results.split("\n");
  splitResults.forEach((line) => {
    // JS destructuring is like Python's unpacking.
    let [teamOne, teamTwo, outcome] = line.split(";");

    // Create team's entries in the tally object if necessary.
    if (tally[teamOne] === undefined) {
      tally = createNewTeam(tally, teamOne);
      teamNames.push(teamOne);
    }
    if (tally[teamTwo] === undefined) {
      tally = createNewTeam(tally, teamTwo);
      teamNames.push(teamTwo);
    }

    // Update values.
    switch (outcome) {
      case "win":
        tally[teamOne].W += 1;
        tally[teamTwo].L += 1;
        break;
      case "loss":
        tally[teamOne].L += 1;
        tally[teamTwo].W += 1;
        break;
      case "draw":
        tally[teamOne].D += 1;
        tally[teamTwo].D += 1;
        break;
    }
    tally[teamOne].MP += 1;
    tally[teamTwo].MP += 1;
  });

  // Tally up the points for each team.
  for (let team in tally) {
    tally[team].P = tally[team].W * 3 + tally[team].D;
  }

  // Sort the team names by points in descending order.
  teamNames.sort((a, b) => tally[b].P - tally[a].P);

  // Ties need to be sorted alphabetically.
  const sortTies = (a, b) => {
    if (tally[a].P === tally[b].P) {
      return a < b ? -1 : 1;
    } else {
      // Keep existing order (a negative value would also work).
      return 0;
    }
  };
  teamNames.sort(sortTies);

  // For iterating over each team's stats.
  // "P" is left out because it is formatted differently.
  const headings = ["MP", "W", "D", "L"];

  // Convert object to string output.
  teamNames.forEach((team) => {
    output += "\n" + team.padEnd(31, " ") + "|";

    headings.forEach((key) => {
      output += tally[team][key]?.toString().padStart(3, " ") + " |";
    });

    output += tally[team].P?.toString().padStart(3, " ");
  });

  return output;
};
