"""Pytest tests for pnpm-install.py script.

Specifically, it tests the directory identification function.
"""

from pathlib import Path

import pytest

import pnpm_install


@pytest.fixture
def installable_dirs():
    return [path.name for path in pnpm_install.find_dirs()]


@pytest.mark.parametrize(
    ("dir_name"),
    [
        "anagram",
        "flatten-array",
        "food-chain",
        "isbn-verifier",
        "pascals-triangle",
        "prime-factors",
        "queen-attack",
    ],
)
def test_install_in_dir(installable_dirs: list[str], dir_name: str):
    assert dir_name in installable_dirs


@pytest.mark.parametrize(
    ("dir_name"),
    [
        "annalyns-infiltration",
        "bird-watcher",
        "lasagna",
        "leap",
        "mixed-juices",
        "poetry-club-door-policty",
    ],
)
def test_do_not_install_in_dir(installable_dirs: list[str], dir_name: str):
    assert dir_name not in installable_dirs
