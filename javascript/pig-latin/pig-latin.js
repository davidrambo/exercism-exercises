/**
 * @param {any} text The word to be turned into pig latin.
 * @param {any} index Starting character to include.
 * @returns {String} text[0:index] transposed to end of text[index:]
 */
export const transposeAtIndex = (text, index) => {
  return text.slice(index) + text.slice(0, index);
};

/** Uses a regular expression to determine the subscript at which the
 * piglatin string should begin.
 *
 * [aeio] = one of any of the listed vowels
 *
 * xr | yt = start with either xr or yt
 *
 * (?<!q)u = a look-behind to match a "u" not preceded by a "q"
 *
 * (?<=\wy) = a look-behind to match a y only when preceded by another character
 *
 * @param {any} text the string to be converted into piglatin
 */
export const locateSplitIndex = (text) => {
  const re = /[aeio]|xr|yt|(?<!q)u|(?<=\w)y/i;
  return text.search(re);
};

/** Looking at slaymance's solution, this can be rendered into a more
 * functional style by using map:
 *   text
 *     .split(" ")
 *     .map( word => `${transposeAtIndex(word, locateSplitIndex(word))}ay`)
 *     .join(" ");
 * @param {any} text phrase to be converted into piglatin
 * @returns {String} phrase in piglatin
 */
export const translate = (text) => {
  // Split the phrase into discrete words.
  let words = text.split(" ");

  // Convert them one by one.
  let piglatinWords = Array(words.length);
  let idx = 0;
  for (const word of words) {
    let splitIndex = locateSplitIndex(word);
    piglatinWords[idx++] = transposeAtIndex(word, splitIndex) + "ay";
  }
  return piglatinWords.join(" ");
};
