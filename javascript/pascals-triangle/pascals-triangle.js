/**
 * @param {any} n number of rows to create
 * @returns {array[array[number]]} an array of array of numbers, where each
 * nested array represents a row in Pascal's triangle.
 */
export const rows = (n) => {
  if (n === 0) return [];

  let triangle = [[1]];

  let prevRow = Array;
  let num = 0;

  // New row elements are computed by adding the previous row at the same index
  // to the previous row at that index - 1.
  for (let i = 1; i < n; i++) {
    prevRow = triangle[i - 1];
    let newRow = [];

    for (let j = 0; j < prevRow.length; j++) {
      num = prevRow[j];
      if (j > 0) {
        num += prevRow[j - 1];
      }
      newRow.push(num);
    }
    newRow.push(1);
    triangle.push(newRow);
  }

  return triangle;
};
