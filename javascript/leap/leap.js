/**
 * Returns true if the given year is a leap year.
 *
 * @param {any} year the year in question
 */
export const isLeap = (year) => {
  return year % 4 === 0 && (year % 100 !== 0 || year % 400 === 0);
};
