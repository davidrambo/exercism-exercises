export const gigasecond = (startDate) => {
  let newDate = new Date(startDate.getTime() + 1e12);
  // Date.UTC() time is measured in ms.

  return newDate;
};
