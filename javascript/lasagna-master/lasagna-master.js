/// <reference path="./global.d.ts" />
// @ts-check

export function cookingStatus(remainingTime) {
  if (typeof remainingTime === "number" && remainingTime === 0) {
    return "Lasagna is done.";
  }
  if (!remainingTime || Number.isNaN(remainingTime)) {
    return "You forgot to set the timer.";
  }

  return "Not done, please wait.";
}

export function preparationTime(layers, avgPrep = 2) {
  return layers.length * avgPrep;
}

export function quantities(layers) {
  let noodles = 0;
  let sauce = 0;

  for (const ingr of layers) {
    if (ingr === "noodles") {
      noodles += 50;
    } else if (ingr === "sauce") {
      sauce += 0.2;
    }
  }
  return { noodles, sauce };
}

export function addSecretIngredient(friendsList, myList) {
  const secret = friendsList[friendsList.length - 1];
  myList.push(secret);
}

/**
 * @param {any} recipe holds amounts for 2 portions
 * @param {any} portions desired number of portions
 * @returns {object} recipe object holding amounts for `porrtions` portions
 */
export function scaleRecipe(recipe, portions) {
  let scaled = {};
  const scale = portions / 2;
  for (const ingr in recipe) {
    scaled[ingr] = recipe[ingr] * scale;
  }
  return scaled;
}
