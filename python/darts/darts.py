import math


def score(x: float | int, y: float | int) -> int:
    """Determines the score of a dart by calculating the distance from the board's
    center.

    Args:
        x: x-coordinate of dart
        y: y-coordinate of dart

    Returns:
        score
    """
    distance = math.sqrt(x**2 + y**2)

    # The distance checks must be increasingly narrowed, since repeated boolean
    # evaluations will update the value for that key.
    points: dict[bool:int] = {
        distance <= 10: 1,
        distance <= 5: 5,
        distance <= 1: 10,
    }

    return points.get(True, 0)
