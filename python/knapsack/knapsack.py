def maximum_value(maximum_weight: int, items: list[dict[str, int]]) -> int:
    """Determines the maximum value of items that can be taken within the weight limit.

    To solve via dynamic programming, fill out a matrix comprising each item and each
    integer weight within the limit as the two axes:
       i,w|0|1|2|3|4|...|w
         0|0|0|0|0|0| 0 |0
         1|0|0|0|0|3| 3 |3
         2|0|1|1|1|3| 4 |3
         3|0|0|5|5|5| 5 |5
       ...| | | | | |   |
         n| | | | | |   |

    Initialize the matrix with 0s. Let's say item 3 has a value 5 and a weight 2. Then
    5 is entered at m[3,2] unless a greater value can be found in m[1..2, 2]. That is,
    if some combination of items 1 and 2 could have greater value at that weight. We
    see that at i=2 and w=4, the previous item's best value at that weight is 3, which
    is greater than item #2's value (val=1, weight=1). Once the weight value increase
    to 5 or greater, then the value can incorporate m[2-1,5-1] = m[1, 4] = 3. Thus,
    m[2,5] = 4.

    Args:
        maximum_weight: Limit weight of items taken.
        items: List of items represented as dicts with a `value` and a `weight`.

    Returns:
        Maximum value.
    """
    items_count = len(items)
    matrix = [[0 for _ in range(maximum_weight + 1)] for _ in range(items_count + 1)]

    for i, item in enumerate(items, start=1):
        value = item["value"]
        weight = item["weight"]

        for w in range(maximum_weight + 1):
            """If the current item weighs more than the current weight value considered,
            then fill in value with previous item at that weight. If it is less, then
            fill in with the max of:
                - value at previous item of the same weight considered;
                - current item's value plus the value at previous item in remaining
                  weight cell.
            """
            matrix[i][w] = (
                matrix[i - 1][w]
                if weight > w
                else max(matrix[i - 1][w], matrix[i - 1][w - weight] + value)
            )
    return matrix[items_count][maximum_weight]
