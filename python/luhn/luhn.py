class Luhn:
    def __init__(self, card_num):
        self.digits = []
        for item in card_num:
            try:
                self.digits.append(int(item))
            except ValueError:
                if item != " ":
                    self.digits = [0]
                    break
                else:
                    pass

    def valid(self):
        if len(self.digits) < 2:
            return False

        luhn_val = 0

        # double every other digit, starting from the right, subtracting 9 if > 9
        for idx, digit in enumerate(reversed(self.digits)):
            print(f">>> {id=}, {digit=}")
            new_val = digit
            if idx % 2 == 1:
                new_val *= 2
                print(f"new_val * 2 = {new_val}")
                if new_val > 9:
                    new_val -= 9
            print(f"Adding {new_val} to luhn_val...\n")
            luhn_val += new_val

        return luhn_val % 10 == 0
