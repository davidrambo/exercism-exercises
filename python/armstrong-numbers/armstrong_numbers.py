import itertools


def is_armstrong_number(number: int) -> bool:
    """Determines whether a given number is an Armstrong number.

    Args:
        number: the number in question

    Returns:
        True if the number is the sum of its own digits each raised to the power of
        the number of digits.
    """
    # digits = next(n for n in itertools.count(start=1) if number // (10**n) == 0)
    # After looking at community solutions, I see that I as already converting `number`
    # to a string. So I ought to take advantage of that to find the number of digits.
    num_str = str(number)
    digits = len(num_str)
    return number == sum(int(n) ** digits for n in num_str)
