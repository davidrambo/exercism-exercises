import re


def abbreviate(words):
    # words_list = re.split(r"\s|-|_", words)
    # acronym_list = [w[0].upper() for w in words_list if len(w) > 0]
    # return "".join(acronym_list)

    return "".join(word[0].upper() for word in re.findall(r"[a-zA-Z']+", words))
