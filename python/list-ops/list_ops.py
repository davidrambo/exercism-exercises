def append(list1, list2):
    return list1 + list2


def concat(lists):
    if lists == []:
        return []
    first = lists[0]
    for part in lists[1:]:
        first += part
    return first


def filter(function, list):
    return [item for item in list if function(item)]


def length(list):
    tally = 0
    for _ in list:
        tally += 1
    return tally


def map(function, list):
    return [function(item) for item in list]


def foldl(function, list, initial):
    result = initial
    if list == []:
        return initial
    for item in list:
        result = function(result, item)
    return result


def foldr(function, list, initial):
    result = initial
    if list == []:
        return initial
    for item in reversed(list):
        result = function(result, item)
    return result


def reverse(list):
    if list == []:
        return []
    return list[::-1]
