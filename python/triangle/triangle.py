import itertools


def is_triangle(sides: list[int]) -> bool:
    """Determines whether the sides represent a valid triangle.

    Args:
        sides: the three lengths of a triangle

    Returns:
        True if it is a valid triangle, else False.
    """
    if not all(side > 0 for side in sides):
        return False

    a, b, c = sides

    if a + b < c:
        return False
    if b + c < a:
        return False
    if a + c < b:
        return False

    return True


def equilateral(sides: list[int]) -> bool:
    """Determines whether the triangle with three `sides` is equilateral.

    Since an equilateral triangle's three sides must all be equal, by the
    transitive property, if a == b and b == c, then a == c. So only two comparisons
    need be made. itertools.pairwise([a, b, c]) generates (a, b) and (b, c).

    Another way to have done this is to convert sides to a set and check its len().
    I do this in the next procedure.

    Args:
        sides : values representing the three sides of a triangle

    Returns:
        True if all three sides are equal.
    """
    return is_triangle(sides) and all(a == b for a, b in itertools.pairwise(sides))


def isosceles(sides: list[int]) -> bool:
    """Determines whether the triangle with three `sides` is isosceles.

    We use set() to remove duplicates and

    Args:
        sides : values representing the three sides of a triangle

    Returns:
        bool : True if at least two values in sides are true.
    """
    return is_triangle(sides) and len(set(sides)) < 3


def scalene(sides: list[int]) -> bool:
    """Determines whether the triangle with three `sides` is scalene.

    Again, this could be accomplished with len(set(sides)) == 3.
    But I wanted to take a different approach for each function.

    Args:
        sides: values representing the three sides of a triangle

    Returns:
        True if all sides are different.
    """
    return is_triangle(sides) and all(
        a != b for a, b in itertools.combinations(sides, 2)
    )
