"""Functions used in preparing Guido's gorgeous lasagna.

Learn about Guido, the creator of the Python language:
https://en.wikipedia.org/wiki/Guido_van_Rossum

This is a module docstring, used to describe the functionality
of a module and its functions and/or classes.
"""

EXPECTED_BAKE_TIME = 40
PREPARATION_TIME = 2


def bake_time_remaining(elapsed_bake_time):
    """Calculate the bake time remaining.

    :param elapsed_bake_time: int - baking time already elapsed.
    :return: int - remaining bake time (in minutes) derived from 'EXPECTED_BAKE_TIME'.

    Function that takes the actual minutes the lasagna has been in the oven as
    an argument and returns how many minutes the lasagna still needs to bake
    based on the `EXPECTED_BAKE_TIME`.
    """

    return EXPECTED_BAKE_TIME - elapsed_bake_time


def preparation_time_in_minutes(number_of_layers: int) -> int:
    """Returns the number of minutes it takes to prepare a lasagna with the
    given number of layers.

    Args:
        number_of_layers: the number of layers in the lasagna

    Returns:
        total prep time in minutes
    """
    return PREPARATION_TIME * number_of_layers


def elapsed_time_in_minutes(number_of_layers: int, elapsed_bake_time: int) -> int:
    """Returns the total time spent on the lasagna: prep and bake.

    Args:
        number_of_layers: number of layers in the lasagna
        elapsed_bake_time: how long it has been in the oven

    Returns:
        prep time plus bake time so far
    """
    return preparation_time_in_minutes(number_of_layers) + elapsed_bake_time
