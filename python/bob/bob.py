def response(hey_bob: str) -> str:
    """Determines a response based on the string.

    Args:
        hey_bob: The input string that determines Bob's response.

    Returns:
        Bob's response as a string.
    """
    hey_bob = hey_bob.strip()

    if hey_bob == "":
        return "Fine. Be that way!"

    is_yell = len(hey_bob) > 1 and hey_bob.isupper()

    if hey_bob.endswith("?"):
        if is_yell:
            return "Calm down, I know what I'm doing!"
        else:
            return "Sure."
    if is_yell:
        return "Whoa, chill out!"

    return "Whatever."
