def reverse(text: str) -> str:
    result = ""
    for letter in reversed(text):
        result += letter
    return result