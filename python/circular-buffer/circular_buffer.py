class BufferFullException(BufferError):
    """Exception raised when CircularBuffer is full.

    message: explanation of the error.

    """

    def __init__(self, message):
        self.message = message


class BufferEmptyException(BufferError):
    """Exception raised when CircularBuffer is empty.

    message: explanation of the error.

    """

    def __init__(self, message):
        self.message = message


class CircularBuffer:
    """A circular buffer of size `capacity`.

    Attributes:
        _buffer (list) : the buffer of size `capacity`
        _size (int) : number of elements in the buffer
        _capacity (int) : capacity of the buffer
        _oldest (int) : index of the oldest element
        _back (int) : index next write operation

    """

    def __init__(self, capacity):
        self._buffer = list(None for x in range(capacity))
        self._size = 0
        self._capacity = capacity
        self._oldest = 0
        self._back = 0

    def read(self):
        """Pops the elements from the front of the buffer and returns it."""
        if self._size == 0:
            raise BufferEmptyException("Circular buffer is empty")

        data = self._buffer[self._oldest]

        self._size -= 1
        self._advance_oldest()

        return data

    def _advance_oldest(self):
        """Helper function to advance the index of the oldest element."""
        if self._oldest == self._capacity - 1:
            self._oldest = 0
        else:
            self._oldest += 1

    def write(self, data):
        """Writes `data` to the back of the buffer, if there is space."""
        if self._size == self._capacity:
            raise BufferFullException("Circular buffer is full")

        self._buffer[self._back] = data
        self._size += 1

        if self._back == self._capacity - 1:
            self._back = 0
        else:
            self._back += 1

        return None

    def overwrite(self, data):
        """Writes data over the front element if full, otherwise just writes."""
        if self._size < self._capacity:
            return self.write(data)

        self._buffer[self._oldest] = data

        self._advance_oldest()

    def clear(self):
        """Resets the buffer."""
        self._front = self._back = self._size = 0
