"""Functions to automate Conda airlines ticketing system."""

import itertools

# from typing import Generator
# According to the official docs, this is a deprecated alias to:
from collections.abc import Generator


def generate_seat_letters(number: int) -> Generator:
    """Generate a series of letters for airline seats.

    :param number: int - total number of seat letters to be generated.
    :return: generator - generator that yields seat letters.

    Seat letters are generated from A to D.
    After D it should start again with A.

    Example: A, B, C, D
    """
    letters = itertools.cycle("ABCD")
    for _ in range(number):
        yield next(letters)


def generate_seats(number: int) -> Generator:
    """Generate a series of identifiers for airline seats.

    :param number: int - total number of seats to be generated.
    :return: generator - generator that yields seat numbers.

    A seat number consists of the row number and the seat letter.

    There is no row 13.
    Each row has 4 seats.

    Seats should be sorted from low to high.

    Example: 3C, 3D, 4A, 4B

    """
    # Calculate highest number row
    max_row: int = number // 4
    max_row += 1 if max_row >= 13 else 0  # skip row 13
    max_row += 1 if number % 4 > 0 else 0  # seats for a non-full row

    # Generator expression for row numbers.
    rows = (row for row in range(1, max_row + 1) if row != 13 for _ in range(4))

    for row, letter in zip(rows, generate_seat_letters(number)):
        # Since zip stops with the first of its argument iterators to stop, there
        # should be a way to indefinitely generate row numbers and to stop with
        # the seat letter generator.
        yield str(row) + letter


def assign_seats(passengers: list[str]) -> dict[str:str]:
    """Assign seats to passengers.

    :param passengers: list[str] - a list of strings containing names of passengers.
    :return: dict - with the names of the passengers as keys and seat numbers as values.

    Example output: {"Adele": "1A", "Björk": "1B"}

    """
    return {
        name: seat for name, seat in zip(passengers, generate_seats(len(passengers)))
    }


def generate_codes(seat_numbers: list[str], flight_id: str) -> Generator:
    """Generate codes for a ticket.

    :param seat_numbers: list[str] - list of seat numbers.
    :param flight_id: str - string containing the flight identifier.
    :return: generator - generator that yields 12 character long ticket codes.

    """
    for seat in seat_numbers:
        yield (seat + flight_id).ljust(12, "0")
