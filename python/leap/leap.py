def leap_year(year: int) -> bool:
    match (year % 4 == 0, year % 100 == 0, year % 400 == 0):
        case (True, True, True):
            return True
        case (True, True, False):
            return False
        case (True, False, False):
            return True
    return False