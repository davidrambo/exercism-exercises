import re


def proteins(strand: str):
    codon_protein_map = {
        "AUG": "Methionine",
        "UUU": "Phenylalanine",
        "UUC": "Phenylalanine",
        "UUA": "Leucine",
        "UUG": "Leucine",
        "UCU": "Serine",
        "UCC": "Serine",
        "UCA": "Serine",
        "UCG": "Serine",
        "UAU": "Tyrosine",
        "UAC": "Tyrosine",
        "UGU": "Cysteine",
        "UGC": "Cysteine",
        "UGG": "Tryptophan",
        "UAA": False,
        "UAG": False,
        "UGA": False,
    }

    proteins = list()

    codons = re.findall(r"(\w{3})", strand)

    for codon in codons:
        if codon_protein_map[codon] is False:
            break
        proteins.append(codon_protein_map[codon])

    return proteins
