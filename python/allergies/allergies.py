class Allergies:
    allergens = (
        ("cats", 128),
        ("pollen", 64),
        ("chocolate", 32),
        ("tomatoes", 16),
        ("strawberries", 8),
        ("shellfish", 4),
        ("peanuts", 2),
        ("eggs", 1),
    )

    def __init__(self, score: int):
        self._allergens = [
            allergen for (allergen, value) in self.allergens if score & value != 0
        ]

    def allergic_to(self, item: str) -> str:
        return item in self._allergens

    @property
    def lst(self) -> list:
        return self._allergens
