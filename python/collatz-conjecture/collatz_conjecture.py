def steps(number: int) -> int:
    """Calculates the number of steps taken for a given number to reach 1 by way
    of the Collatz Conjecture.

    Args:
        number: positive, non-zero integer to be processed to 1

    Returns:
        number of steps taken
    """
    if number < 1:
        raise ValueError("Only positive integers are allowed")

    steps = 0
    while number != 1:
        if number % 2 == 0:  # even
            number //= 2
        else:
            number = 3 * number + 1
        steps += 1

    return steps
