def is_pangram(sentence: str) -> bool:
    """Determines whether the sentence is a pangram.

    Args:
        sentence: String of characters.

    Returns:
        True if all 26 letters in the English alphabet are in the sentence.
    """
    alpha_set: set[str] = set(letter.lower() for letter in sentence if letter.isalpha())
    return len(alpha_set) == 26
