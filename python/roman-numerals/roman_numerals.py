def roman(number):
    """Converts arabic numeral `number` to Roman numerals."""
    roman_numeral_map = (
        ("M", 1000),
        ("CM", 900),
        ("D", 500),
        ("CD", 400),
        ("C", 100),
        ("XC", 90),
        ("L", 50),
        ("XL", 40),
        ("X", 10),
        ("IX", 9),
        ("V", 5),
        ("IV", 4),
        ("I", 1),
    )

    roman = ""

    for numeral, arabic in roman_numeral_map:
        while number >= arabic:
            number -= arabic
            roman += numeral

    return roman
