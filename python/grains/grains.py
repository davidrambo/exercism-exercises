TOTAL_GRAINS = 2**64 - 1


def square(number: int) -> int:
    """Returns the number of wheat grains on a chess board square, where the first contains
    1 grain, and each contains double the last.

    Args:
        number (int): the square number, must be between 1 and 64, inclusive

    Raises:
        ValueError: when `number` falls outside [1,64] range

    Returns:
        number of grains of wheat on that square
    """
    if number < 1 or number > 64:
        raise ValueError("square must be between 1 and 64")
    return 2 ** (number - 1)


def total() -> int:
    """Returns the total number of grains of wheat on the chess board.

    Summation of 2^i, from i=0 to n=63, is 2^(n+1) - 1.

    Returns:
        TOTAL_GRAINS, which is 2**64 - 1.
    """
    return TOTAL_GRAINS
