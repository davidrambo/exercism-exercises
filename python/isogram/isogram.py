import string


def is_isogram(phrase: str) -> bool:
    """Determines whether the given string is an isogram.

    Args:
        phrase: a string

    Returns:
        True if the string is an isogram, else False.
    """
    alphas_only: list[str] = [
        letter for letter in phrase.lower() if letter in string.ascii_lowercase
    ]
    return len(alphas_only) == len(set(alphas_only))
