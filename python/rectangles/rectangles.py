def is_contiguous_horizontal(line: str, start: int, end: int) -> bool:
    """Returns True if no spaces or vertical marks exist in `line` from start to end."""

    for c in line[start : end + 1]:
        if c == " " or c == "|":
            return False
    return True


def is_contiguous_vertical(strings: list[str], col: int, start: int, end: int) -> bool:
    """Returns True if no spaces or horizontal marks exist in the column of characters
    in strings from start to end.
    """

    for line in strings[start : end + 1]:
        if line[col] == " " or line[col] == "-":
            return False
    return True


def rectangles(strings):
    """Returns the number of rectangles represented by the list of strings.

    For example:
        +-+
        | |
        +-+
    contains one rectangle. And:
        +-+--+
        | |  |
        +-+--+
    contains three.

    These would be represented as:
        ["+-+", "| |", "+-+"]
    and
        ["+-+--+", "| |  |", "+-+--+"]
    respectively.

    Thinking recursively, everytime a '+' is encountered, a new search can initiate.
    A rectangle has been confirmed when another '+' that is equal lengths apart,
    horizontally and vertically, from the the starting '+', without interruption.
    An interruption is a " " (a space).

    An example with interruptions:
        +--+   +-+
        |  |   | |
        +--+---+-+
        +--+---+-+
    contains ten rectangles.

    Args:
        strings (list[str]): represents a 2d collection of lines that compose rectangles

    Returns:
        (int) number of rectangles
    """

    def _search(x: int, y: int):
        """Helper function that provides a starting cursor in the rectangles.

        `x` is the horizontal coordinate, which is a subscript of the string of
        strings[y], where `y` is the vertical coordinate, which is the subscript of
        the list `strings`.
        """

        """Lists of contiguous distances from (x,y) to crosses in `strings`."""
        x_crosses: list[int] = []
        y_crosses: list[int] = []

        # Find all contiguous crosses in current string and mark their position.
        line = strings[y]
        for x_idx in range(x + 1, len(strings[y])):
            if line[x_idx] == "+":
                x_crosses.append(x_idx)
            elif line[x_idx] == " ":
                break

        # Find all crosses in current column (x position).
        for y_idx in range(y + 1, len(strings)):
            line = strings[y_idx]
            if line[x] == "+":
                y_crosses.append(y_idx)
            elif line[x] == " ":
                break

        local_total = 0
        # Iterate over x_crosses and check for a cross that corresponds with index in
        # y_crosses.
        for x_pos in x_crosses:
            for y_pos in y_crosses:
                if strings[y_pos][x_pos] == "+":
                    # Check for contiguity.
                    if is_contiguous_horizontal(
                        strings[y_pos], x, x_pos
                    ) and is_contiguous_vertical(strings, x_pos, y, y_pos):
                        local_total += 1

        return local_total

    total = 0

    for y_idx, _ in enumerate(strings):
        for x_idx, _ in enumerate(strings[y_idx]):
            if strings[y_idx][x_idx] == "+":
                total += _search(x_idx, y_idx)

    return total
