#lang racket

(provide proteins)

(define (translate codon)
  (case codon
    [("AUG") "Methionine"]
    (("UUU" "UUC") "Phenylalanine")
    (("UUA" "UUG") "Leucine")
    (("UCU" "UCC" "UCA" "UCG") "Serine")
    (("UAU" "UAC") "Tyrosine")
    (("UGU" "UGC") "Cysteine")
    (("UGG") "Tryptophan")
    (("UAA" "UAG" "UGA") "STOP")
    (else ("INVALID"))))

;; Creates a list of the next three characters and returns it as a string
(define (next-codon rna)
  (substring rna 0 3))

;; Matches a codon to a protein
#|(define (translate codon)
  (define (iter pairs)
    (cond ((null? pairs) "INVALID")
          ((string=? (caar pairs) codon) (cadar pairs))
          (else (iter (cdr pairs)))))
  (iter codon-map)) |#

(define (proteins strand)
  (define (rna-to-protein rna aminos)
    (if (zero? (string-length rna))
        aminos
        (let ((protein (translate (next-codon rna))))
              (cond ((string=? "STOP" protein) aminos)
                    ((string=? "INVALID" protein) (error "Invalid codon" (next-codon rna)))
                    (else (rna-to-protein (substring rna 3)
                                          (append aminos (list protein))))))))
  (rna-to-protein strand '()))
          

#| (define (proteins strand)
  (define (rna-to-protein rna aminos)
    (cond ((null? rna) aminos)
          ((string=? "STOP" (translate (next-codon rna))) aminos)
          ((string=? "INVALID" (translate (next-codon rna)))
           (error "Invalid codon" strand))
          (else
            (rna-to-protein (cdddr rna)
                            (append aminos
                                    (list (translate (next-codon rna))))))))
  (rna-to-protein (string->list strand) '())) |#
