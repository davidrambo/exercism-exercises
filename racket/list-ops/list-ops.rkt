#lang racket

(provide my-length
         my-reverse
         my-map
         my-filter
         my-fold
         my-append
         my-concatenate)

(define (my-length sequence)
  (if (null? sequence) 0 (+ 1 (my-length (cdr sequence)))))

(define (my-reverse sequence)
  (define (iter acc items)
    (if (null? items)
      acc
      (iter (cons (car items) acc) (cdr items))))
  (iter '() sequence))

(define (my-map operation sequence)
  (if (null? sequence)
    '()
    (cons (operation (car sequence))
          (my-map operation (cdr sequence)))))

(define (my-filter operation? sequence)
  (cond ((null? sequence) '())
        ((operation? (car sequence))
         (cons (car sequence)
               (my-filter operation? (cdr sequence))))
        (else (my-filter operation? (cdr sequence)))))

(define (my-fold operation accumulator sequence)
  (if (null? sequence)
    accumulator
    (operation (car sequence)
               (my-fold operation accumulator (cdr sequence)))))

(define (my-append sequence1 sequence2)
  (define (helper front back)
    (if (null? front)
      back
      (helper (cdr front) (cons (car front) back))))
  (helper (reverse sequence1) sequence2))

(define (my-concatenate sequence-of-sequences)
  (if (null? sequence-of-sequences)
    '()
    (append (car sequence-of-sequences) (my-concatenate (cdr sequence-of-sequences)))))
        
