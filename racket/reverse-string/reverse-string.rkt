#lang racket
(provide my-reverse)

(define (fold-left op init seq)
  (define (iter acc items)
    (if (null? items)
      acc
      (iter (op (car items) acc) (cdr items))))
  (iter init seq))

(define (my-reverse s)
  (list->string (fold-left cons '() (string->list s))))
