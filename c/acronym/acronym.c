#include "acronym.h"
#include <ctype.h>
#include <stdlib.h>
#include <string.h>

#define ACRONYM_LENGTH 20

char *abbreviate(const char *phrase) {
  char *acronym = malloc((sizeof(char) * ACRONYM_LENGTH));

  char *acro_ptr = acronym;

  if (phrase == NULL || strlen(phrase) == 0) {
    return NULL;
  }

  while (*phrase != '\0') {
    // Get to next valid alpha.
    while (!isalpha(*phrase)) {
      phrase++;
    }

    // Add uppercase character.
    *acro_ptr++ = toupper(*phrase);

    // Seek next dividing character.
    while (*phrase != '-' && *phrase != 32 && *phrase != ',' &&
           *phrase != '_') {
      // Advance to next character, return if it is the end.
      if (*phrase++ == '\0') {
        return acronym;
      }
    }
  }
  return acronym;
}
