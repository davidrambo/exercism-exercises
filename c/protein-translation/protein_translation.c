#include "protein_translation.h"

#define AUG 65171
#define UUU 84405
#define UUC 84387
#define UUA 84385
#define UUG 84391
#define UCU 83847
#define UCC 83829
#define UCA 83827
#define UCG 83833
#define UAU 83785
#define UAC 83767
#define UGU 83971
#define UGC 83953
#define UGG 83957
#define UAA 83765
#define UAG 83771
#define UGA 83951

unsigned int hash(const char a, const char b, const char c) {
  unsigned int result = 0;
  result = a;
  result = (31 * result) + b;
  result = (31 * result) + c;

  return result;
}

proteins_t proteins(const char *const rna) {
  int pos = 0;             // for reading rna
  unsigned int codon_hash; // for converting each codon into an int

  proteins_t translation;
  translation.count = 0;
  // Assume valid, will change to false with default switch.
  translation.valid = true;

  if (rna[0] == '\0') {
    return translation;
  }

  while (pos < (MAX_PROTEINS * 3 + 3) && rna[pos] != '\0') {
    codon_hash = 0; // reset the codon int

    // Convert codon into a size_t.
    codon_hash = hash(rna[pos], rna[pos + 1], rna[pos + 2]);
    pos += 3;

    // STOP codes
    if (codon_hash == UAA || codon_hash == UAG || codon_hash == UGA) {
      break;
    }

    switch (codon_hash) {
    case AUG:
      translation.proteins[translation.count++] = Methionine;
      break;
    case UUU:
    case UUC:
      translation.proteins[translation.count++] = Phenylalanine;
      break;
    case UUA:
    case UUG:
      translation.proteins[translation.count++] = Leucine;
      break;
    case UCU:
    case UCC:
    case UCA:
    case UCG:
      translation.proteins[translation.count++] = Serine;
      break;
    case UAU:
    case UAC:
      translation.proteins[translation.count++] = Tyrosine;
      break;
    case UGU:
    case UGC:
      translation.proteins[translation.count++] = Cysteine;
      break;
    case UGG:
      translation.proteins[translation.count++] = Tryptophan;
      break;
    default:
      translation.valid = false;
      break;
    }
  }

  return translation;
}
