#ifndef CIRCULAR_BUFFER_H
#define CIRCULAR_BUFFER_H

#include <errno.h>
#include <stdint.h>
#include <stdlib.h>

typedef int buffer_value_t;

typedef struct {
  size_t oldest;
  size_t back;
  size_t size;
  size_t capacity;
  buffer_value_t *buffer;
} circular_buffer_t;

circular_buffer_t *new_circular_buffer(size_t capacity);

void clear_buffer(circular_buffer_t *buf);

int16_t write(circular_buffer_t *buf, buffer_value_t value);

int16_t read(circular_buffer_t *buf, buffer_value_t *read_value);

int16_t overwrite(circular_buffer_t *buf, buffer_value_t value);

void delete_buffer(circular_buffer_t *buf);

#endif
