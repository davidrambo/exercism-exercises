#include "circular_buffer.h"

circular_buffer_t *new_circular_buffer(size_t capacity) {
  circular_buffer_t *buf = calloc(1, sizeof(circular_buffer_t));

  if (!buf) {
    return NULL;
  }

  buf->buffer = calloc(capacity, sizeof(buffer_value_t));

  if (!buf->buffer) {
    free(buf);
    return NULL;
  }

  buf->oldest = 0;
  buf->back = 0;
  buf->size = 0;
  buf->capacity = capacity;

  return buf;
}

void clear_buffer(circular_buffer_t *buf) {
  buf->size = 0;
  buf->oldest = 0;
  buf->back = 0;
}

int16_t write(circular_buffer_t *buf, buffer_value_t value) {
  if (buf->size == buf->capacity) {
    errno = ENOBUFS; // buffer is full
    return EXIT_FAILURE;
  }

  buf->buffer[buf->back] = value;

  buf->back = (buf->back + 1) % buf->capacity;

  buf->size += 1;

  return EXIT_SUCCESS;
}

int16_t read(circular_buffer_t *buf, buffer_value_t *read_value) {
  if (buf->size == 0) {
    errno = ENODATA;
    return EXIT_FAILURE;
  }

  *read_value = buf->buffer[buf->oldest];

  buf->oldest = (buf->oldest + 1) % buf->capacity;

  buf->size--;

  return EXIT_SUCCESS;
}

int16_t overwrite(circular_buffer_t *buf, buffer_value_t value) {
  if (buf->size < buf->capacity) {
    return write(buf, value);
  }

  buf->buffer[buf->oldest] = value;

  buf->oldest = (buf->oldest + 1) % buf->capacity;

  return EXIT_SUCCESS;
}

void delete_buffer(circular_buffer_t *buf) {
  free(buf->buffer);
  free(buf);
}
