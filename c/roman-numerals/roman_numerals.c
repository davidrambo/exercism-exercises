#include "roman_numerals.h"
#include <stdlib.h>
#include <string.h>

// I think the longest roman numeral is 823, which is 12 characters.
// Correction: from bobahop's solution, it's 15 characters.
#define BUFFER_LENGTH 16

char *to_roman_numeral(unsigned int number) {
  char *result;
  result = (char *)malloc(sizeof(char) * BUFFER_LENGTH);
  if (result == NULL) {
    return NULL;
  }

  // Without initializing the first char slot in the string, I was getting
  // incorrect output for numbers > 1.
  result[0] = '\0';

  const unsigned arabics[13] = {1000, 900, 500, 400, 100, 90, 50,
                                40,   10,  9,   5,   4,   1};
  const char romans[13][3] = {"M",  "CM", "D",  "CD", "C",  "XC", "L",
                              "XL", "X",  "IX", "V",  "IV", "I"};

  int a, r;
  a = 0;
  r = 0;

  while (number > 0) {
    if (number >= arabics[a]) {
      number = number - arabics[a];
      strncat(result, romans[r], 2);
    } else {
      a++;
      r++;
    }
  }

  return result;
}
