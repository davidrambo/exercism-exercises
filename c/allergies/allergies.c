#include "allergies.h"

/* is_allergic_to:
 * Since each allergen's contribution to the decimal-number score corresponds to
 * a bit in its binary-number representation, we can use bitwise operators.
 * To check for an allergy, this function can check whether the bit in the
 * corresponding position is set. For example, let's say the score is 12d, or
 * 1100b, which indicates allergies to shellfish and strawberries. To check
 * whether this score indicates an allergy to peanuts, the following would
 * need to be calculated:
 *   (1100 & 0010)
 * which returns 0000b. To set the second binary bit, we can start with 1,
 * i.e. 0001b, and left-shift it by 1 to become 0010b. Since each allergen
 * corresponds to an int (enum typedef of allergen_t), the allergen parameter
 * provides this.
 */
bool is_allergic_to(allergen_t allergen, int score) {
  return ((1 << allergen) & score);
}

allergen_list_t get_allergens(int score) {
  allergen_list_t allergens;
  allergens.count = 0;

  for (allergen_t allergy_num = ALLERGEN_EGGS; allergy_num < ALLERGEN_COUNT;
       allergy_num++) {
    allergens.allergens[allergy_num] = is_allergic_to(allergy_num, score);

    if (allergens.allergens[allergy_num])
      allergens.count++;
  }

  return allergens;
}
