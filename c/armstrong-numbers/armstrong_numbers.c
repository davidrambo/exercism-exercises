#include "armstrong_numbers.h"

/* Returns true if candidate is an Armstrong number. */
bool is_armstrong_number(int candidate) {

  // single-digit integers are all Armstrong numbers
  if (candidate < 10)
    return true;

  // First, find the number of digits.
  int digits = 0; // alt. = log10(candidate) + 1;  // #include <math.h>
  int numref = candidate;

  while (numref > 0) {
    digits++;
    numref /= 10;
  }

  // Calculate the value
  numref = candidate;
  int total = 0;      // to track the sum
  int exp_temp;       // for the current digit
  int exp_temp_total; // for tallying the exponentiation of exp_temp
  for (int d = 0; d < digits; d++) {
    // Set up variables for current digit.
    exp_temp_total = 1;
    exp_temp = numref % 10;

    // Perform exponentiation: exp_temp^digits
    for (int i = 0; i < digits; i++) {
      exp_temp_total *= exp_temp;
    }

    total += exp_temp_total; // Tally it
    numref /= 10;            // Move to next digit
  }

  return candidate == total;
}
