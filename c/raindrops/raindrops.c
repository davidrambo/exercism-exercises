#include "raindrops.h"
#include <stdio.h>
#include <string.h>

char *convert(char raindrops[], int number) {
  int rained = 0;

  if (number % 3 == 0) {
    strncat(raindrops, "Pling", 6);
    rained = 1;
  }
  if (number % 5 == 0) {
    strncat(raindrops, "Plang", 6);
    rained = 1;
  }
  if (number % 7 == 0) {
    strncat(raindrops, "Plong", 6);
    rained = 1;
  }

  if (rained == 0) {
    sprintf(raindrops, "%d", number);
  }

  return raindrops;
}
