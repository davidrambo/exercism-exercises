#include "reverse_string.h"
#include <stdlib.h>
#include <string.h>

/* returns a given string in reverse */
char *reverse(const char *src) {

  const char *front = src;

  char *result;
  result = malloc(sizeof(char) * strlen(src));

  char *end = result + (sizeof(char) * strlen(src));
  *end = '\0';
  end--;

  for (; *front != '\0'; front++) {
    *end = *front;
    end--;
  }

  return result;
}
