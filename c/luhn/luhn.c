#include "luhn.h"
#include <stdbool.h>

/* luhn: Performs a Luhn checksum on a string.
 *
 * After getting a pointer to the end of the string, it makes one pass.
 * If the char is a space, it is skipped, and the digit counter is unchanged.
 * If the char cannot be converted to an int, then return false.
 * Otherwise, the digit is treated according to the checksum procedure.
 *
 * This is a pretty barebones approach: it doesn't create a clean array of ints,
 * nor does it take advantage of C's ctype and string libraries. It relies on
 * the ol' fashioned ASCII table.
 */
bool luhn(const char *num) {
  int digit_counter = 0, luhn_sum = 0, idx = 0, luhn_digit = 0;
  const char *ptr = num;

  // Get to the null pointer.
  // Track length of num at the same time.
  while (*ptr) {
    ptr++;
    idx++;
  }

  --ptr; // Point to last character.
  --idx;

  // Too few digits.
  // Note: There could be leading non-numeric characters.
  // Hence the check at the end for the digit_counter.
  if (idx < 1)
    return false;

  while (idx >= 0) {
    // Skip spaces
    if (*ptr == 0x20) {
      --ptr;
      --idx;
      continue;
    }

    // False if non-numeric
    if (*ptr < 0x30 || *ptr > 0x39)
      return false;

    // Get digit
    luhn_digit = *ptr - 0x30;

    --ptr;
    --idx;

    if (digit_counter % 2 == 0) {
      luhn_sum += luhn_digit;
    } else {
      luhn_digit *= 2;
      if (luhn_digit > 9)
        luhn_digit -= 9;
      luhn_sum += luhn_digit;
    }

    ++digit_counter;
  }

  // To handle leading spaces.
  if (digit_counter < 2)
    return false;

  if (luhn_sum % 10 == 0)
    return true;

  return false;
}
