#include "list_ops.h"
#include <stdio.h>
#include <stdlib.h>

/** Creates a new list_t of size `length` out of the provided elements. */
list_t *new_list(size_t length, list_element_t elements[]) {
  list_t *new_list = malloc(sizeof(list_t) + length * sizeof(list_element_t));

  if (new_list == NULL) {
    printf("Failed to allocate memory for a new list.\n");
    exit(1);
  }

  new_list->length = length;

  int i = 0;
  for (int *p = elements; p < elements + length; p++) {
    new_list->elements[i++] = *p;
  }

  return new_list;
}

/** Destroys a list, leaving a dangling pointer. */
void delete_list(list_t *list) {
  free(list);
}

/** Creates a new list_t with list1's elements first, followed by
 * list2's elements.
 */
list_t *append_list(list_t *list1, list_t *list2) {
  size_t new_length = list1->length + list2->length;
  list_t *result_list =
      malloc(sizeof(list_t) + new_length * sizeof(list_element_t));
  if (result_list == NULL) {
    exit(1);
  }
  result_list->length = new_length;

  int idx = 0;
  list_element_t *p;

  // Copy over list1's elements.
  for (p = list1->elements; p < list1->elements + list1->length; p++) {
    result_list->elements[idx++] = *p;
  }

  // Copy over list2's elements.
  for (p = list2->elements; p < list2->elements + list2->length; p++) {
    result_list->elements[idx++] = *p;
  }

  return result_list;
}

list_t *filter_list(list_t *list, bool (*filter)(list_element_t)) {
  // First create an array the size of the list's length.
  list_element_t temp_arr[list->length];

  // Fill it with the filtered elements.
  int new_length = 0;
  for (size_t idx = 0; idx < list->length; idx++) {
    if (filter(list->elements[idx])) {
      temp_arr[new_length++] = list->elements[idx];
    }
  }

  // Then use this array and the known number of filtered elements to create
  // a new list_t.
  list_t *filtered = new_list(new_length, temp_arr);
  return filtered;
}

size_t length_list(list_t *list) {
  return list->length;
}

list_t *map_list(list_t *list, list_element_t (*map)(list_element_t)) {
  list_t *mapped =
      malloc(sizeof(list_t) + list->length * sizeof(list_element_t));
  if (mapped == NULL) {
    exit(1);
  }
  mapped->length = list->length;

  for (size_t idx = 0; idx < list->length; idx++) {
    mapped->elements[idx] = map(list->elements[idx]);
  }

  return mapped;
}

/** Performs the given foldr procedure on elements in a list from left to
 * right. For example, division and [6, 2, 3] -> ((6/2) / 3) = (3/3) = 1.
 * And division and [3, 2, 6] -> ((3/2) / 6) = (1/6) = 0.
 * These examples take the first element as the initial.
 */
list_element_t foldl_list(list_t *list, list_element_t initial,
                          list_element_t (*foldr)(list_element_t,
                                                  list_element_t)) {
  for (size_t idx = 0; idx < list->length; idx++) {
    initial = foldr(initial, list->elements[idx]);
  }

  return initial;
}

/** Reduces the given list from the right.
 * For the list [2, 5], division, and an initial of 5,
 * (2 / (5 / 5)) = (2 / 1) = 2.
 * Note that this preserves the order of elements as the order in which
 * operands are passed to the procedure. In other words, it is not:
 * (5 / 5) / 2.
 * */
list_element_t foldr_list(list_t *list, list_element_t initial,
                          list_element_t (*foldr)(list_element_t,
                                                  list_element_t)) {
  // Handle empty list.
  if (list->length == 0) {
    return initial;
  }

  for (int idx = list->length - 1; idx >= 0; idx--) {
    initial = foldr(list->elements[idx], initial);
  }

  return initial;
}

list_t *reverse_list(list_t *list) {
  list_t *reversed =
      malloc(sizeof(list_t) + list->length * sizeof(list_element_t));
  if (reversed == NULL) {
    exit(1);
  }
  reversed->length = list->length;

  list_element_t *forward_ptr = list->elements;
  list_element_t *reverse_ptr = reversed->elements + list->length - 1;

  for (size_t counter = 0; counter < list->length; counter++) {
    *reverse_ptr-- = *forward_ptr++;
  }

  return reversed;
}
