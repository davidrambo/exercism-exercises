"""By AmjadHD on github:
https://github.com/exercism/cli/issues/718#issuecomment-1475031765

"""

import asyncio
import json
import requests


async def download_exercise(exercise: str, track: str, overwrite):
    cmd = f"exercism download --exercise={exercise} --track={track}"
    if overwrite:
        cmd += " --force"
    proc = await asyncio.create_subprocess_shell(
        cmd, stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.PIPE
    )

    stdout, stderr = await proc.communicate()

    print(f"[{cmd!r} exited with {proc.returncode}]")
    if stdout:
        print(f"[stdout]\n{stdout.decode()}")
    if stderr:
        print(f"[stderr]\n{stderr.decode()}")


async def download_track(track: str, overwrite=False):
    url = f"https://github.com/exercism/{track}/raw/main/config.json"
    content = json.loads(requests.get(url).content)

    await asyncio.gather(
        *(
            download_exercise(exercise["slug"], track, overwrite)
            for exercise in content["exercises"]["practice"]
        )
    )


asyncio.run(download_track("python"))
